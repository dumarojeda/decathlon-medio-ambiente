function mapSpain() {
  $('.pin').on('click', function () {
    console.log('algo')
    var tienda = $(this).data('tienda');
    var puntos = $(this).data('puntos');
    var img = $(this).data('img');
    var text = $(this).data('text');
    var template = "<div class='title-action'>"+
        "Acciones que realiza"+
        "<div class='store'>"+
          tienda+
        "</div>"+
      "</div>"+
      "<div class='points-action'>"+
        "<div class='point'>"+puntos+"</div>"+
        "<div class='text'>puntos</div>"+
      "</div>"+
      "<div class='content'>"+
        "<img src='"+img+"'>"+
        "<p>"+text+"</p>"+
      "</div>";

    $('.pin').removeClass('active');
    $(this).addClass('active');
    $('.chart-action').addClass('col-sm-4');
    $('.chart-action').html(template);
  });
}
function scrolling() {
  $(window).scroll(function(){
    var offSetPosition = $(window).scrollTop();
    //console.log(offSetPosition)
    if(offSetPosition < 240) {
      $('.page-links.scrolling ul li').removeClass();
      $('.page-links.scrolling ul li').eq(0).addClass('active');
    } else if(offSetPosition < 780) {
      $('.page-links.scrolling ul li').removeClass();
      $('.page-links.scrolling ul li').eq(1).addClass('active');
    } else if(offSetPosition < 1300) {
      $('.page-links.scrolling ul li').removeClass();
      $('.page-links.scrolling ul li').eq(2).addClass('active');
    } else {
      $('.page-links.scrolling ul li').removeClass();
      $('.page-links.scrolling ul li').eq(3).addClass('active');
    }
  });

  $('.page-links ul li').hover(
    function() {
      $(this).addClass("hover");
      $(this).parent().addClass("hovering");
    }, function() {
      $(this).removeClass("hover");
      $(this).parent().removeClass("hovering");
    }
  );
}
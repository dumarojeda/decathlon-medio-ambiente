//= require jquery
//= require jquery-validate
//= require slick-carousel
//= require_tree .
$(document).ready(function () {
  menu();
  validateForm();
  resizeDiv();
  scrolling();
  carousel();
  mapSpain();
  inputCustom();
});


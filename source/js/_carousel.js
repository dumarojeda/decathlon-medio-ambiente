function carousel() {
  $('.carousel').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: '<div class="slick-prev slick-arrow icon-back"></div>',
    nextArrow: '<div class="slick-next slick-arrow icon-next"></div>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 2
        }
      }
    ]
  });
}